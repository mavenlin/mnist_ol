import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds

def preprocess(x):
  return (tf.cast(x["image"], tf.float32) / 255., x["label"])

class MnistOL(object):
  def __init__(self, percent=1.):
    train_dataset, test_dataset = tfds.load(
        "mnist", split=[f"train[:{int(percent*100)}%]", "test"])
    self.train_dataset = train_dataset.map(preprocess)
    self.test_dataset = test_dataset.map(preprocess)

  def train(self, batch=1):
    return self.train_dataset.batch(batch)

  def test(self, batch=128):
    return self.test_dataset.batch(batch)
